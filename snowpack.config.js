/** @type {import("snowpack").SnowpackUserConfig } */
// eslint-disable-next-line no-undef
module.exports = {
  mount: {
    src: '/',
    public: '/public/'
  },
  alias: {
    '@': './src'
  },
  plugins: ['@snowpack/plugin-sass']
};
